stages:
  - test

Java:
  stage: test
  image: registry.gitlab.com/hplar/learning:java
  before_script:
    - cd java
  script:
    - mvn $MVN_CLI_OPTS clean org.jacoco:jacoco-maven-plugin:prepare-agent test jacoco:report
    - awk -F"," '{ instructions += $4 + $5; covered += $5 } END { print covered, "/", instructions, "instructions covered"; print 100*covered/instructions, "% covered" }' target/site/jacoco/jacoco.csv
    - cov=$(awk -F"," '{ instructions += $4 + $5; covered += $5 } END { print covered, "/", instructions, "instructions covered"; print 100*covered/instructions, "% covered" }' target/site/jacoco/jacoco.csv | tail -n -1 | cut -d ' ' -f 1 | xargs printf "%.2f")
    - anybadge -v $cov% -l Java-coverage -f java-cov.svg
  artifacts:
    reports:
      junit:
        - java/target/surefire-reports/TEST-*.xml
    paths:
      - java/java-cov.svg
    when: always
    expire_in: 30 days
  coverage: '/\d+.\d+ \% covered/'
  only:
    refs:
      - merge_requests
    changes:
      - .gitlab-ci.yml
      - java/**/*

Python:
  stage: test
  image: registry.gitlab.com/hplar/learning:python
  before_script:
    - cd python
    - export PYTHONPATH="src"
    - poetry config virtualenvs.create false
    - poetry install
  script:
    - coverage run -m pytest --junitxml=report.xml
    - coverage report
    - coverage xml
    - cov=$(coverage report | tail -n -1 | awk '{ print $4 }')
    - anybadge -v $cov -l Python-coverage -f python-cov.svg
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: python/coverage.xml
      junit: python/report.xml
    paths: 
      - python/python-cov.svg
    when: always
    expire_in: 30 days 
  coverage: '/^TOTAL.+?(\d+\%)$/'
  only:
    refs:
      - merge_requests
    changes:
      - .gitlab-ci.yml
      - python/**/*

Rust:
  stage: test
  image: registry.gitlab.com/hplar/learning:rust
  before_script:
    - cd rust
    - cargo build
  script:
    - cargo test -- -Z unstable-options --format json --report-time | cargo2junit > report.xml
    - cargo tarpaulin --out Xml > coverage.txt
    - anybadge -v $(tail -n -1 coverage.txt | awk '{ print $1 }') -l "Rust coverage" -f rust-cov.svg
    - tail -n -1 coverage.txt
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: rust/cobertura.xml
      junit: rust/report.xml
    paths:
      - rust/rust-cov.svg
    when: always
    expire_in: 30 days
  coverage: '/^\d+.\d+% coverage/'
  only:
    refs:
      - merge_requests
    changes:
      - .gitlab-ci.yml
      - rust/**/*

C++:
  stage: test
  image: registry.gitlab.com/hplar/learning:cpp
  before_script:
    - mkdir -p cpp/build
    - cd cpp/build
    - cmake ..
    - make
  script:
    - ctest -V --output-junit report.xml
    - cov=`gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml -r ../src .`
    - gcovr --print-summary -r ../src .
    - anybadge -v $(echo $cov | head -n 1 | awk '{ print $2 }') -l "C++ coverage" -f cpp-cov.svg
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: cpp/build/coverage.xml
      junit: cpp/build/report.xml
    paths:
      - cpp/build/cpp-cov.svg
    when: always
    expire_in: 30 days
  coverage: /^\s*lines:\s*\d+.\d+\%/
  only:
    refs:
      - merge_requests
    changes:
      - .gitlab-ci.yml
      - cpp/**/*

Web:
  stage: test
  image: registry.gitlab.com/hplar/learning:web
  before_script:
    - cd web
    - poetry config virtualenvs.create false
    - poetry install
  script:
    - pytest -vv
  only:
    refs:
      - merge_requests
    changes:
      - .gitlab-ci.yml
      - web/**/*

JS:
  stage: test
  image: registry.gitlab.com/hplar/learning:js
  before_script:
    - cd js
  script:
    - npm install
    - npm test
    - export cov=`npm test | grep -i "all files" | cut -d "|" -f 4`
    - anybadge -v $cov  -l "Javascript coverage" -f js-cov.svg
  coverage: /All\sfiles.*?\s+(\d+.\d+)/
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: js/coverage/cobertura-coverage.xml
      junit: js/junit.xml
    paths:
      - js/js-cov.svg
    when: always
    expire_in: 30 days
  only:
    refs:
      - merge_requests
    changes:
      - .gitlab-ci.yml
      - js/**/*
