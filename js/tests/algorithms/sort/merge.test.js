const mergeSort = require("../../../src/algorithms/sort/merge");

describe("Test merge sort", () => {

  test("It sorts ints", () => {
    const numbers = [34, 64, 8, 2, 98, 12, 0, 43];
    const sorted = [...numbers].sort((a, b) => { return a - b });
    expect(mergeSort(numbers)).toStrictEqual(sorted);
  })
})
