const quickSort = require("../../../src/algorithms/sort/quick");


describe("Test quick sort", () => {

    test("It sorts ints", () => {
        const numbers = [34, 64, 8, 2, 98, 12, 0, 43];
        const sorted = [...numbers].sort((a, b) => { return a - b });
        expect(quickSort(numbers)).toStrictEqual(sorted);
    })
})
