const bubbleSort = require("../../../src/algorithms/sort/bubble");

describe("Test bubble sort", () => {

  test("It sorts ints", () => {
    const numbers = [34, 64, 8, 2, 98, 12, 0, 43];
    const sorted = [...numbers].sort((a, b) => { return a - b });
    expect(bubbleSort(numbers)).toStrictEqual(sorted);
  })
})