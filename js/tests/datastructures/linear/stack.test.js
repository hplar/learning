const { Stack, EmptyStackError } = require("../../../src/datastructures/linear/stack");


describe("Test stack", () => {
    test("empty", () => {
        const stack = new Stack();
        expect(stack.length).toBe(0);
    })

    test("pop", () => {
        const stack = new Stack();
        stack.push(1);
        stack.push(2);
        stack.pop();
        expect(stack.length).toBe(1);
        stack.pop();
        expect(stack.length).toBe(0);
    })

    test("pop empty", () => {
        const stack = new Stack();
        expect(() => stack.pop()).toThrow(EmptyStackError)
    })

    test("peek", () => {
        const stack = new Stack();
        stack.push(1);
        stack.push(2);
        expect(stack.peek()).toBe(2);
        stack.pop();
        expect(stack.peek()).toBe(1);
    })

    test("peek empty", () => {
        const stack = new Stack();
        expect(() => stack.peek()).toThrow(EmptyStackError)
    })

    test("add", () => {
        const stack = new Stack();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        expect(stack.length).toBe(3);
    })
})