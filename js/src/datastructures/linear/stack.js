class EmptyStackError extends Error {
    constructor(message) {
        super(message);
        this.name = "EmptyStackError";
    }
}

class StackNode {
    constructor(val, prev = null) {
        this.val = val;
        this.prev = prev;
    }
}

class Stack {
    constructor() {
        this.end = null;
    }

    push(val) {
        if (this.end === null) {
            this.end = new StackNode(val);
        }

        this.end = new StackNode(val, this.end);

        return this.end.val;
    }

    pop() {
        if (this.end === null) {
            throw new EmptyStackError();
        }

        const popped = this.end;
        this.end = popped.prev;

        return popped.val;
    }

    peek() {
        if (this.end === null) {
            throw new EmptyStackError();
        }

        return this.end.val;
    }

    get length() {
        let length = 0;

        if (this.end === null) {
            return length;
        }

        let node = this.end;
        while (node.prev !== null) {
            node = node.prev;
            length += 1;
        }

        return length;
    }
}

module.exports = {
    Stack,
    EmptyStackError
};