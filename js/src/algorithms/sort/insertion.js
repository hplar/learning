const insertionSort = (arr) => {
    for (let i=0; i < arr.length; i++) {
        const curr = arr[i];

        while (i > 0 && arr[i - 1] > curr) {
            arr[i] = arr[i - 1];
            i = i - 1;
        }
        arr[i] = curr;
    }
    return arr;
}

module.exports = insertionSort;