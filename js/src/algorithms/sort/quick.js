const quickSort = (arr) => {

    let higher = [];
    let lower= [];
    const pivots = [];

    if (arr.length <= 1) {
        return arr;
    }

    const pivot = arr[Math.floor(Math.random() * arr.length)];

    for (let val of arr) {
        if (val < pivot) {
            lower.push(val);
        } else if (val > pivot) {
            higher.push(val);
        } else {
            pivots.push(val);
        }
    }

    higher = quickSort(higher);
    lower = quickSort(lower);

    return [...lower, ...pivots, ...higher];
}

module.exports = quickSort;