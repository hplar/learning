import asyncio

import pytest

from src.aio.aio_queue import AIO


class TestAIOQueue:
    @pytest.mark.parametrize(
        "rounds, sleep_time",
        [
            (1, 0),
            (10, 0),
            (100, 0),
            (1000, 0),
            (10000, 0),
            (100000, 0),
        ],
    )
    async def test_queue(self, rounds, sleep_time):
        aio = AIO(rounds=rounds, sleep_time=sleep_time)
        numbers = await aio.start()
        assert numbers == [i for i in range(0, rounds)]
