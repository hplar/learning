import pytest

from src.books.fluent_python.pythonic_objects.vector2d import Vector2d


class TestVector2d:
    def test_init(self):
        v1 = Vector2d(3.0, 4.0)
        assert v1.x == 3.0
        assert v1.y == 4.0

    def test_repr(self):
        v1 = Vector2d(3.0, 4.0)
        assert repr(v1) == "Vector2d(3.0, 4.0)"

    def test_str(self):
        v1 = Vector2d(3.0, 4.0)
        assert str(v1) == "(3.0, 4.0)"

    def test_bytes(self):
        v1 = Vector2d(3.0, 4.0)
        assert bytes(v1) == (
            b"d\x00\x00\x00\x00\x00\x00\x08@\x00\x00\x00\x00\x00\x00\x10@"
        )

    def test_eq_ok(self):
        v1 = Vector2d(3.0, 4.0)
        v2 = Vector2d(3.0, 4.0)
        assert v1 == v2

    def test_eq_nok(self):
        v1 = Vector2d(3.0, 4.0)
        v2 = Vector2d(1.0, 2.0)
        assert v1 != v2

    def test_eq_nok_type(self):
        v1 = Vector2d(3.0, 4.0)
        assert v1 != b"123"

    def test_abs(self):
        v1 = Vector2d(3.0, 4.0)
        assert abs(v1) == 5.0

    def test_bool(self):
        v1 = Vector2d(3.0, 4.0)
        assert bool(v1) is True

    def test_add_ok(self):
        v1 = Vector2d(3.0, 4.0)
        v2 = Vector2d(1.0, 2.0)
        assert v1 + v2 == Vector2d(4.0, 6.0)

    def test_add_nok(self):
        v1 = Vector2d(3.0, 4.0)
        v2 = "a"
        with pytest.raises(TypeError):
            v1 + v2

    def test_sub_ok(self):
        v1 = Vector2d(3.0, 4.0)
        v2 = Vector2d(1.0, 2.0)
        assert v1 - v2 == Vector2d(2.0, 2.0)

    def test_sub_nok(self):
        v1 = Vector2d(3.0, 4.0)
        v2 = 1.0
        with pytest.raises(TypeError):
            v1 - v2

    def test_mul_ok(self):
        v1 = Vector2d(3.0, 4.0)
        assert v1 * 3 == Vector2d(9.0, 12.0)

    def test_mul_nok(self):
        v1 = Vector2d(3.0, 4.0)
        with pytest.raises(TypeError):
            v1 * "a"

    def test_from_bytes(self):
        v1 = Vector2d(3.0, 4.0)
        assert Vector2d.from_bytes(bytes(v1)) == v1

    @pytest.mark.parametrize(
        "vec2d, fmt, expected",
        [
            (Vector2d(3, 4), "", "(3.0, 4.0)"),
            (Vector2d(3, 4), ".2f", "(3.00, 4.00)"),
            (Vector2d(3, 4), ".3e", "(3.000e+00, 4.000e+00)"),
            (Vector2d(1, 1), "p", "<1.4142135623730951, 0.7853981633974483>"),
            (Vector2d(1, 1), ".3ep", "<1.414e+00, 7.854e-01>"),
            (Vector2d(1, 1), "0.5fp", "<1.41421, 0.78540>"),
        ],
    )
    def test_format(self, vec2d, fmt, expected):
        assert format(vec2d, fmt) == expected

    def test_angle(self):
        v1 = Vector2d(10, 1)
        assert v1.angle() == 0.09966865249116202
