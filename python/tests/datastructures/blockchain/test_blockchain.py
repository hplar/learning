from unittest.mock import patch

from datastructures.blockchain.blockchain import Block, BlockChain


class TestBlockchain:
    def test_genesis_block(self):
        bc = BlockChain()

        last_block = bc.last_block
        assert last_block.index == 0
        assert last_block.data == ["GENESIS BLOCK"]
        assert last_block.prev_hash is None

    def test_construct_block(self):
        bc = BlockChain()

        last_block = bc.last_block
        last_proof_no = last_block.proof_no
        proof_no = bc.construct_proof_of_work(last_proof_no)

        bc.new_data("NEW DATA")
        block = bc.construct_block(proof_no, last_block.hash)
        assert block.index == 1
        assert block.data == ["NEW DATA"]
        assert block.prev_hash == last_block.hash

    def test_check_validity_ok(self):
        bc = BlockChain()

        prev_block = bc.last_block
        last_proof_no = prev_block.proof_no
        proof_no = bc.construct_proof_of_work(last_proof_no)

        bc.new_data("NEW DATA")
        block = bc.construct_block(proof_no, prev_block.hash)
        assert bc.check_validity(block, prev_block)

    def test_check_validity_bad_block_index(self):
        bc = BlockChain()
        block_zero = bc.last_block

        last_proof_no = block_zero.proof_no
        proof_no = bc.construct_proof_of_work(last_proof_no)
        bc.new_data("NEW DATA")
        block_one = bc.construct_block(proof_no, block_zero.hash)

        last_proof_no = block_one.proof_no
        proof_no = bc.construct_proof_of_work(last_proof_no)
        bc.new_data("NEW DATA2")
        block_two = bc.construct_block(proof_no, block_one.hash)

        assert not bc.check_validity(block_two, block_zero)

    def test_check_validity_bad_prev_hash(self):
        bc = BlockChain()
        block_zero = bc.last_block
        last_proof_no = block_zero.proof_no
        proof_no = bc.construct_proof_of_work(last_proof_no)
        bc.new_data("NEW DATA")
        block_one = bc.construct_block(proof_no, block_zero.hash)

        proof_no = bc.construct_proof_of_work(last_proof_no)
        block_with_bad_prev_hash = bc.construct_block(proof_no, "badhash")

        assert not bc.check_validity(block_with_bad_prev_hash, block_one)

    def test_check_validity_bad_proof(self):
        bc = BlockChain()
        block_zero = bc.last_block
        last_proof_no = block_zero.proof_no

        proof_no = bc.construct_proof_of_work(last_proof_no)
        bc.new_data("NEW DATA")
        block_with_bad_proof_no = bc.construct_block(proof_no, block_zero.hash)
        block_with_bad_proof_no.proof_no = 1337

        assert not bc.check_validity(block_with_bad_proof_no, block_zero)

    def test_check_validity_bad_timestamp(self, monkeypatch):
        block = Block(index=0, proof_no=0, prev_hash="0123", data=["0"], timestamp=10)

        block_with_bad_timestamp = Block(
            index=1, proof_no=1, prev_hash=block.hash, data=["1"], timestamp=8
        )

        def mock_verify(block_one, block_two):
            return True

        monkeypatch.setattr(BlockChain, "verifying_proof", mock_verify)
        assert not BlockChain.check_validity(block_with_bad_timestamp, block)
