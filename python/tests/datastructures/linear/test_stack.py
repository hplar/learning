import pytest

from datastructures.linear.stack import Stack, StackNode


class TestStack:
    def test_push_returns_stack_node(self):
        s = Stack()
        assert type(s.push(1)) == StackNode

    def test_push_has_stack_order(self):
        s = Stack()

        a = s.push("Dostoevsky")
        assert s.end == a
        assert s.end.prev is None

        b = s.push("Gogol")
        assert s.end == b
        assert s.end.prev == a

        c = s.push("Solzhenitsyn")
        assert s.end == c
        assert s.end.prev == b

    def test_value_of_nodes(self):
        s = Stack()

        a = s.push("The quieter")
        b = s.push("you become, ")
        c = s.push("the more you")
        d = s.push("are able to hear.")

        assert a.val == "The quieter"
        assert b.val == "you become, "
        assert c.val == "the more you"
        assert d.val == "are able to hear."

    def test_pop_has_stack_order(self):
        s = Stack()
        a = s.push("Bernard Lowe")
        b = s.push("Robert Ford")
        c = s.push("Dolores Abernathy")
        assert s.end == c

        s.pop()
        assert s.end == b

        s.pop()
        assert s.end == a

        s.pop()
        assert s.end is None

    def test_pop_return_node(self):
        s = Stack()
        a = s.push("1.0_1-hellofriend.wav")
        b = s.push("1.0_2-oneincontrol.aiff")
        c = s.push("1.1_2-wearefsociety.sd2")

        assert s.pop() == c
        assert s.pop() == b
        assert s.pop() == a

    def test_pop_on_empty_stack(self):
        s = Stack()

        with pytest.raises(Exception):
            s.pop()

    def test_is_empty(self):
        s = Stack()
        assert s.is_empty() is True

    def test_is_empty_on_populated_stack(self):
        s = Stack()
        s.push("Hunter S. Thompson")

        assert s.is_empty() is False
