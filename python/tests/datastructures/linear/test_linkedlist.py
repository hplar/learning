import pytest

from datastructures.linear.linkedlist import (
    DoublyLinkedNode,
    EmptyListException,
    LinkedList,
    RootExistsException,
)


class TestLinkedList(object):
    def test_set_root(self):
        l = LinkedList()
        root = DoublyLinkedNode("Rickmancing the stone")

        linked_list_root = l.set_root(root)
        assert linked_list_root == root

    def test_set_root_when_root_exists(self):
        l = LinkedList()
        l.set_root(DoublyLinkedNode("Close Rick-counters of the Rick Kind"))

        with pytest.raises(RootExistsException):
            l.set_root(DoublyLinkedNode("Lawnmower Dog"))

    def test_insert_when_list_has_root(self):
        l = LinkedList()
        root = l.set_root(DoublyLinkedNode("Get Schwifty"))

        A = l.insert(root, "Total Rickall")
        assert l.root.next == A

    def tests_insert_when_list_has_no_root(self):
        l = LinkedList()

        with pytest.raises(EmptyListException):
            l.insert(DoublyLinkedNode("A Rickle"), "in Time")

    def test_insert_has_location(self):
        l = LinkedList()
        root = l.set_root(DoublyLinkedNode("Ricksy Business"))

        A = l.insert(root, "The Ricks Must Be Crazy")
        assert root.next == A
        assert A.prev == root

        B = l.insert(root, "Mortynight Run")
        assert root.next == B
        assert B.prev == root
        assert B.next == A
        assert A.prev == B

    def test_search_existing_node(self):
        l = LinkedList()
        l.set_root(DoublyLinkedNode("vlees"))

        A = l.insert(l.root, "kaas")
        assert list(l.search("kaas")) == [A]

    def test_search_non_existing_node(self):
        l = LinkedList()
        l.set_root(DoublyLinkedNode("taart"))

        assert list(l.search("henkie")) == []

    def test_search_multiple_nodes(self):
        l = LinkedList()

        root = l.set_root(DoublyLinkedNode("worst"))
        A = l.insert(root, "worst")
        B = l.insert(A, "worst")
        C = l.insert(B, "worst")
        assert list(l.search("worst")) == [root, A, B, C]

    def test_search_with_start_node(self):
        l = LinkedList()
        root = l.set_root(DoublyLinkedNode("ceci n'est pas une root."))

        A = l.insert(l.root, "kaas")
        B = l.insert(A, "worst")
        C = l.insert(B, "kaas")
        assert list(l.search("kaas", root)) == [A, C]
        assert list(l.search("kaas", B)) == [C]

    def test_remove_returns_node(self):
        l = LinkedList()
        l.set_root(DoublyLinkedNode(""))

        A = l.insert(l.root, "Zaphod Beeblebrox")
        assert l.remove(A) == A

    def test_removed_node_has_pointers_removed(self):
        l = LinkedList()
        l.set_root(DoublyLinkedNode("Ford Prefect"))
        A = l.insert(l.root, "Artur Dent")
        B = l.insert(A, "Trillian")

        assert A.prev == l.root
        assert A.next == B

        removed_A = l.remove(A)
        assert removed_A.prev is None
        assert removed_A.next is None

    def test_remove_has_list_order(self):
        l = LinkedList()
        root = l.set_root(DoublyLinkedNode("root"))
        A = l.insert(root, "first")
        B = l.insert(A, "second")

        l.remove(A)
        assert root.next == B
        assert B.prev == root

    def test_remove_on_empty_list(self):
        l = LinkedList()

        with pytest.raises(EmptyListException):
            l.remove(DoublyLinkedNode("this should raise an exception"))

    def test_remove_root_node(self):
        l = LinkedList()
        root = l.set_root(DoublyLinkedNode("root"))
        l.remove(root)
        assert l.root is None

    def test_remove_root_node_with_extra_nodes(self):
        l = LinkedList()
        root = l.set_root(DoublyLinkedNode("root"))
        future_root = l.insert(root, "future_root")

        assert l.remove(root) == root
        assert future_root == l.root

    def test_search_empty_linked_list(self):
        l = LinkedList()

        with pytest.raises(EmptyListException):
            node = next(l.search("nope"))
