import pytest

from datastructures.trees.binarysearchtree import (
    BinarySearchTreeNode,
    EmptyTreeException,
    NonStackIterativeBinarySearchTree,
    RecursiveBinarySearchTree,
    StackIterativeBinarySearchTree,
    AbstractBinarySearchTree,
)


class BinarySearchTreeTester(object):
    tree_type = None

    def test_insert(self):
        b = self.tree_type()
        b.insert(BinarySearchTreeNode("a"))

        new = BinarySearchTreeNode("b")
        assert b.insert(new) == new

    def test_insert_has_tree_order(self):
        b = self.tree_type()
        root = b.insert(BinarySearchTreeNode(10))

        A = b.insert(BinarySearchTreeNode(2))
        assert root.left == A
        assert A.parent == root

        B = b.insert(BinarySearchTreeNode(20))
        assert root.right == B
        assert root.left == A
        assert B.parent == root

        C = b.insert(BinarySearchTreeNode(1))
        assert root.left.left == C
        assert A.left == C
        assert C.parent == A

        D = b.insert(BinarySearchTreeNode(17))
        assert root.right.left == D
        assert B.left == D
        assert D.parent == B

        E = b.insert(BinarySearchTreeNode(19))
        assert root.right.left.right == E
        assert D.right == E
        assert E.parent == D

    def test_insert_without_root(self):
        b = self.tree_type()
        root = b.insert(BinarySearchTreeNode(200))

        assert b.root == root

    def test_remove_node_last_in_first_out(self):
        b = self.tree_type()
        root = b.insert(BinarySearchTreeNode(10))

        A = b.insert(BinarySearchTreeNode(5))
        B = b.insert(BinarySearchTreeNode(20))
        C = b.insert(BinarySearchTreeNode(1))
        D = b.insert(BinarySearchTreeNode(2))
        E = b.insert(BinarySearchTreeNode(15))

        removed_E = b.remove(E)
        assert removed_E == E

        removed_D = b.remove(D)
        assert removed_D == D

        removed_C = b.remove(C)
        assert removed_C == C

        removed_B = b.remove(B)
        assert removed_B == B

        removed_A = b.remove(A)
        assert removed_A == A

        removed_root = b.remove(root)
        assert removed_root == root

    def test_remove_root_with_only_root(self):
        b = self.tree_type()
        root = b.insert(BinarySearchTreeNode(10))

        assert b.remove(root) == root
        assert b.root is None

    def test_remove_root_with_populated_tree(self):
        b = self.tree_type()

        root = b.insert(BinarySearchTreeNode(10))
        A = b.insert(BinarySearchTreeNode(5))
        B = b.insert(BinarySearchTreeNode(20))
        C = b.insert(BinarySearchTreeNode(9))

        assert b.remove(root) == root
        assert b.root == A
        assert b.root.right == C
        assert b.root.right.right == B

    def test_remove_on_empty_tree(self):
        b = self.tree_type()

        with pytest.raises(EmptyTreeException):
            b.remove(BinarySearchTreeNode("Kill it! Before it lays eggs!"))

    def test_traverse_empty_tree(self):
        b = self.tree_type()
        b.root = None

        with pytest.raises(EmptyTreeException):
            next(b.traverse(BinarySearchTreeNode(1)))

    def test_traverse(self):
        b = self.tree_type()
        A = b.insert(BinarySearchTreeNode(20))
        B = b.insert(BinarySearchTreeNode(10))
        C = b.insert(BinarySearchTreeNode(30))
        D = b.insert(BinarySearchTreeNode(5))
        E = b.insert(BinarySearchTreeNode(15))
        F = b.insert(BinarySearchTreeNode(25))
        G = b.insert(BinarySearchTreeNode(1))
        H = b.insert(BinarySearchTreeNode(13))

        result = list(b.traverse(A))
        result_val = [x.val for x in result]

        exp_result = [G, D, B, H, E, A, F, C]
        exp_result_val = [1, 5, 10, 13, 15, 20, 25, 30]

        assert result_val == exp_result_val
        assert result == exp_result


class TestRecursiveBinarySearchTree(BinarySearchTreeTester):
    tree_type = RecursiveBinarySearchTree


class TestStackIterativeBinarySearchTree(BinarySearchTreeTester):
    tree_type = StackIterativeBinarySearchTree


class TestNonStackIterativeBinarySearchTree(BinarySearchTreeTester):
    tree_type = NonStackIterativeBinarySearchTree
