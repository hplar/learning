import datetime

from algorithms.sort.bubble import bubble_sort
from algorithms.sort.insertion import insertion_sort
from algorithms.sort.merge import merge_sort
from algorithms.sort.quick import quick_sort


class SharedTests:
    sort_method = None

    def test_ints(self):
        expected_result = [3, 23, 43, 999, 1246, 10000]
        test_data = [3, 1246, 23, 999, 10000, 43]
        returned_data = self.sort_method(test_data)
        assert returned_data == expected_result

    def test_floats(self):
        expected_result = [0.01, 0.1, 1.0, 10.0, 12.0]
        test_data = [0.1, 10.0, 12.0, 1.0, 0.01]
        returned_data = self.sort_method(test_data)
        assert returned_data == expected_result

    def test_floats_ints(self):
        expected_result = [0.001, 0.01, 10.0, 32, 90]
        test_data = [90, 10.0, 32, 0.01, 0.001]
        returned_data = self.sort_method(test_data)
        assert returned_data == expected_result

    def test_empty_list(self):
        expected_result = []
        test_data = []
        returned_data = self.sort_method(test_data)
        assert returned_data == expected_result

    def test_strings(self):
        expected_result = ["a", "cake", "is", "lie", "the"]
        test_data = ["the", "cake", "is", "a", "lie"]
        returned_data = self.sort_method(test_data)
        assert returned_data == expected_result

    def test_datetime(self):
        rick_and_morty_season_3_release_date = datetime.date(2017, 7, 30)
        guy_fawkes_birthday = datetime.date(1570, 4, 13)
        mayan_end_of_the_world = datetime.date(2012, 12, 21)

        expected_result = [
            guy_fawkes_birthday,
            mayan_end_of_the_world,
            rick_and_morty_season_3_release_date,
        ]

        test_data = [
            mayan_end_of_the_world,
            rick_and_morty_season_3_release_date,
            guy_fawkes_birthday,
        ]

        returned_data = self.sort_method(test_data)
        assert returned_data == expected_result

    def test_list_of_lists(self):
        expected_result = [[0, 9, 8], [1, 2, 3], [10, 15, 12]]
        test_data = [[1, 2, 3], [0, 9, 8], [10, 15, 12]]
        returned_data = self.sort_method(test_data)
        assert returned_data == expected_result

    def test_sorted_list_of_lists(self):
        expected_result = [[0, 1, 5], [1, 3, 4], [2, 4, 5]]
        test_data = [[0, 1, 5], [1, 3, 4], [2, 4, 5]]
        returned_data = self.sort_method(test_data)
        assert returned_data == expected_result

    def test_list_of_tuples(self):
        expected_result = [(0, 5, 4), (1, 4, 9), (3, 6, 7), (8, 7, 4, 5)]
        test_data = [(1, 4, 9), (0, 5, 4), (8, 7, 4, 5), (3, 6, 7)]
        returned_data = self.sort_method(test_data)
        assert returned_data == expected_result

    def test_sorted_list_of_tuples(self):
        expected_result = [(0, 5, 4), (1, 4, 9), (3, 6, 7), (8, 7, 4, 5)]
        test_data = [(0, 5, 4), (1, 4, 9), (3, 6, 7), (8, 7, 4, 5)]
        returned_data = self.sort_method(test_data)
        assert returned_data == expected_result

    def test_list_of_booleans(self):
        expected_result = [False, False, True, True, True]
        test_data = [True, False, True, True, False]
        returned_data = self.sort_method(test_data)
        assert returned_data == expected_result


class TestBubbleSorter(SharedTests):
    sort_method = staticmethod(bubble_sort)


class TestInsertionSorter(SharedTests):
    sort_method = staticmethod(insertion_sort)


class TestMergeSorter(SharedTests):
    sort_method = staticmethod(merge_sort)


class TestQuickSorter(SharedTests):
    sort_method = staticmethod(quick_sort)
