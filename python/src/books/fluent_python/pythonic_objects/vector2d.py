from __future__ import annotations

import math
from array import array
from typing import Union, Iterator


class Vector2d:
    """Euclidean vector class as a Pythonic object."""

    typecode: str = "d"

    def __init__(self, x: float, y: float):
        """Initialize a Vector2d object."""
        self.x = float(x)
        self.y = float(y)

    def __iter__(self) -> Iterator:
        """Return an iterator over the components of the vector."""
        return (i for i in (self.x, self.y))

    def __repr__(self) -> str:
        """Return a debug string representation of the vector."""
        class_name = type(self).__name__
        return "{}({!r}, {!r})".format(class_name, *self)

    def __str__(self) -> str:
        """Return a string representation of the vector."""
        return str(tuple(self))

    def __bytes__(self) -> bytes:
        """Return a bytes representation of the vector."""
        return bytes([ord(self.typecode)]) + bytes(array(self.typecode, self))

    def __abs__(self) -> float:
        """Return the magnitude of the vector."""
        return math.hypot(self.x, self.y)

    def __bool__(self) -> bool:
        """Return True if the vector is not the origin."""
        return bool(abs(self))

    def __eq__(self, vec2d: Vector2d) -> Union[bool, NotImplementedError]:
        """Return True if the vector is equal to the other vector."""
        if isinstance(vec2d, Vector2d):
            return tuple(self) == tuple(vec2d)
        else:
            return NotImplemented

    def __add__(self, vec2d: Vector2d) -> Union[Vector2d, NotImplementedError]:
        """Return the sum of the vector and the other vector."""
        if isinstance(vec2d, Vector2d):
            return Vector2d(self.x + vec2d.x, self.y + vec2d.y)
        else:
            return NotImplemented

    def __sub__(self, vec2d: Vector2d) -> Union[Vector2d, NotImplementedError]:
        """Return the difference of the vector and the other vector."""
        if isinstance(vec2d, Vector2d):
            return Vector2d(self.x - vec2d.x, self.y - vec2d.y)
        else:
            return NotImplemented

    def __mul__(self, vec2d: Vector2d) -> Union[Vector2d, NotImplementedError]:
        """Return the product of the vector and the other vector."""
        if isinstance(vec2d, (int, float)):
            return Vector2d(self.x * vec2d, self.y * vec2d)
        else:
            return NotImplemented

    def __format__(self, format_spec=""):
        """Return a formatted string representation of the vector."""
        if format_spec.endswith("p"):
            format_spec = format_spec[:-1]
            coords = (abs(self), self.angle())
            outer_fmt = "<{}, {}>"
        else:
            coords = self
            outer_fmt = "({}, {})"
        components = (format(c, format_spec) for c in coords)
        return outer_fmt.format(*components)

    @classmethod
    def from_bytes(cls, octets: bytes) -> Vector2d:
        """Return a vector from a bytes representation."""
        typecode = chr(octets[0])
        memv = memoryview(octets[1:]).cast(typecode)
        return cls(*memv)

    def angle(self) -> float:
        """Return the angle of the vector."""
        return math.atan2(self.y, self.x)
