import asyncio

from asyncio import Queue
from typing import List


class AIO:
    def __init__(self, rounds: int = 10, sleep_time: float = 0):
        self.rounds: int = rounds
        self.sleep_time: float = sleep_time
        self.queue: asyncio.Queue = asyncio.Queue()

    async def producer(self) -> None:
        for i in range(0, self.rounds):
            await self.queue.put(i)
            await asyncio.sleep(self.sleep_time)

    async def consumer(self) -> List[int]:
        numbers: List[int] = []

        for i in range(0, self.rounds):
            numbers.append(await self.queue.get())
            await asyncio.sleep(self.sleep_time)

            if self.queue.qsize() <= 0:
                break

        return numbers

    async def start(self):
        producer = asyncio.create_task(self.producer())
        numbers = asyncio.create_task(self.consumer())

        await asyncio.gather(producer, numbers)

        return numbers.result()
