from abc import abstractmethod, ABC


class EmptyTreeException(Exception):
    pass


class BinarySearchTreeNode(object):
    def __init__(self, val):
        self.parent = None
        self.left = None
        self.right = None
        self.val = val


class AbstractBinarySearchTree(ABC):
    def __init__(self):
        self.root = None

    def insert(self, node, start_node=None):
        if self.root is None:
            self.root = node
            return self.root

        if start_node is None:
            start_node = self.root

        while start_node:
            if node.val < start_node.val:
                if start_node.left is None:
                    start_node.left = node
                    start_node.left.parent = start_node
                    break
                start_node = start_node.left

            if node.val >= start_node.val:
                if start_node.right is None:
                    start_node.right = node
                    start_node.right.parent = start_node
                    break
                start_node = start_node.right

        return node

    def remove(self, node):
        if self.root is None:
            raise EmptyTreeException

        if node.parent is None:
            self.root = None
        elif node.parent.left == node:
            node.parent.left = None
        else:
            node.parent.right = None

        if node.left is not None:
            self.insert(node.left, start_node=node.parent)
        if node.right is not None:
            self.insert(node.right, start_node=node.parent)

        node.parent = None
        node.left = None
        node.right = None

        return node

    @abstractmethod
    def traverse(self, node):
        """Not implemented"""


class RecursiveBinarySearchTree(AbstractBinarySearchTree):
    def traverse(self, node):
        if self.root is None:
            raise EmptyTreeException

        if node is None:
            return

        for left in self.traverse(node.left):
            yield left
        yield node
        for right in self.traverse(node.right):
            yield right


class StackIterativeBinarySearchTree(AbstractBinarySearchTree):
    def traverse(self, node):
        if self.root is None:
            raise EmptyTreeException

        stack = []

        while True:
            if node is not None:
                stack.append(node)
                node = node.left
            else:
                if len(stack) > 0:
                    node = stack.pop()
                    yield node
                    node = node.right
                else:
                    break


class NonStackIterativeBinarySearchTree(AbstractBinarySearchTree):
    def traverse(self, node):
        if self.root is None:
            raise EmptyTreeException

        next_action = "check_left"

        while True:
            if next_action == "check_left":
                if node.left is not None:
                    node = node.left
                else:
                    next_action = "check_right"

            if next_action == "check_right":
                yield node
                if node.right is not None:
                    node = node.right
                    next_action = "check_left"
                else:
                    next_action = "move_up"

            if next_action == "move_up":
                if node.parent is None:
                    return

                if node is node.parent.left:
                    next_action = "check_right"
                else:  # we come from right
                    next_action = "move_up"

                node = node.parent
