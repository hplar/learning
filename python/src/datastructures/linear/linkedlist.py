class RootExistsException(Exception):
    pass


class EmptyListException(Exception):
    pass


class DoublyLinkedNode(object):
    def __init__(self, val=None):
        self.prev = None
        self.next = None
        self.val = val


class LinkedList(object):
    def __init__(self):
        self.root = None

    def set_root(self, node):
        if self.root:
            raise RootExistsException

        self.root = node

        return self.root

    def insert(self, node, val):
        if not self.root:
            raise EmptyListException

        if node.next:
            tmp = node.next
            node.next = DoublyLinkedNode(val)
            node.next.prev = node
            node.next.next = tmp
            node.next.next.prev = node.next
        else:
            node.next = DoublyLinkedNode(val)
            node.next.prev = node

        return node.next

    def search(self, val, start_node=None):
        if self.root is None:
            raise EmptyListException

        if start_node is None:
            start_node = self.root

        node = start_node

        while node:
            if node.val == val:
                yield node
            node = node.next

    def remove(self, node):
        if not self.root:
            raise EmptyListException

        tmp = node

        if node.next and node.prev:
            node.next.prev = node.prev
            node.prev.next = node.next
        elif node.prev and not node.next:
            node.prev.next = None
        elif node.next and not node.prev:
            self.root = node.next
            self.root.prev = None
        else:
            self.root = None

        tmp.prev = None
        tmp.next = None

        return tmp
