import random


def quick_sort(arr):

    higher = []
    lower = []
    pivots = []

    if len(arr) <= 1:
        return arr

    pivot = arr[random.randint(1, len(arr) - 1)]

    for value in arr:
        if value < pivot:
            lower.append(value)
        elif value > pivot:
            higher.append(value)
        else:
            pivots.append(value)

    higher = quick_sort(higher)
    lower = quick_sort(lower)

    sorted_list = lower + pivots + higher
    return sorted_list
