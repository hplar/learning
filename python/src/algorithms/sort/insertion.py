def insertion_sort(arr):
    for i in range(1, len(arr)):
        current_value = arr[i]

        while i > 0 and arr[i - 1] > current_value:
            arr[i] = arr[i - 1]
            i = i - 1
        arr[i] = current_value

    return arr
