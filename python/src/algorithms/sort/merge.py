def merge_sort(arr):
    if len(arr) > 1:
        l_set = arr[: len(arr) // 2]
        r_set = arr[len(arr) // 2 :]

        merge_sort(l_set)
        merge_sort(r_set)

        i, j, k = 0, 0, 0

        while i < len(l_set) and j < len(r_set):
            if l_set[i] < r_set[j]:
                arr[k] = l_set[i]
                i += 1
            else:
                arr[k] = r_set[j]
                j += 1
            k += 1

        while i < len(l_set):
            arr[k] = l_set[i]
            i += 1
            k += 1

        while j < len(r_set):
            arr[k] = r_set[j]
            j += 1
            k += 1

    return arr
