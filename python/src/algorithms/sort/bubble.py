def bubble_sort(arr):
    last_unsorted_index = len(arr) - 1

    while last_unsorted_index > 0:
        for i in range(last_unsorted_index):
            l_val = arr[i]
            r_val = arr[i + 1]

            if l_val > r_val:
                arr[i], arr[i + 1] = (
                    arr[i + 1],
                    arr[i],
                )

        last_unsorted_index -= 1

    return arr
