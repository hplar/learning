#Makefile

.PHONY: tests

.ONESHELL:


## JAVA
docker/build/java:
	docker build --tag registry.gitlab.com/hplar/learning:java ./java

docker/push/java:
	docker push --tag registry.gitlab.com/hplar/learning:java

docker/test/java:
	docker-compose --log-level ERROR run java


## PYTHON
docker/build/python:
	docker build --tag registry.gitlab.com/hplar/learning:python ./python

docker/push/python:
	docker push --tag registry.gitlab.com/hplar/learning:python

docker/test/python:
	docker-compose --log-level ERROR run python


## RUST
docker/build/rust:
	docker build --tag registry.gitlab.com/hplar/learning:rust ./rust

docker/push/rust:
	docker push --tag registry.gitlab.com/hplar/learning:rust

docker/test/rust:
	docker-compose --log-level ERROR run rust 


## C++
clean/cpp:
	rm -rf cpp/build

build/cpp: clean/cpp
	mkdir cpp/build
	cd cpp/build
	cmake ..
	make

test/cpp: build/cpp
	cd cpp/build 
	ctest -V --output-junit report.xml
	gcovr --print-summary -r ../src .

docker/build/cpp:
	docker build --tag registry.gitlab.com/hplar/learning:cpp ./cpp

docker/push/cpp:
	docker push registry.gitlab.com/hplar/learning:cpp

docker/test/cpp:
	docker-compose run cpp


## Web
docker/build/web:
	docker build --tag registry.gitlab.com/hplar/learning:web ./web

docker/push/web:
	docker push registry.gitlab.com/hplar/learning:web

docker/test/web:
	docker-compose --log-level ERROR run web


## JS
docker/build/js:
	docker build --tag registry.gitlab.com/hplar/learning:js ./js

docker/push/js:
	docker push registry.gitlab.com/hplar/learning:js

docker/test/js:
	docker-compose --log-level ERROR run js



## COMBINED
docker/build: docker/build/java docker/build/python docker/build/rust docker/build/cpp docker/build/web docker/build/js
docker/push: docker/build/java docker/build/python docker/build/rust docker/push/cpp docker/push/web docker/push/js
docker/tests: docker/test/java docker/test/python docker/test/rust docker/test/cpp docker/test/web docker/test/js
