fn toggle(bool: bool) -> bool {
    !bool
}

fn add(a: f64, b: f64) -> f64 {
    a + b
}

fn multiply(a: u32, b: u32) -> u32 {
    a * b
}

fn subtract(a: i32, b: i32) -> i32 {
    a - b
}

fn is_ascii(c: char) -> bool {
    c.is_ascii()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_toggle() {
        assert!(!toggle(true))
    }

    #[test]
    fn test_add() {
        assert_eq!(add(3.4, 4.5), 7.9)
    }

    #[test]
    fn test_multiply() {
        assert_eq!(multiply(2, 2), 4)
    }

    #[test]
    fn test_subtract() {
        assert_eq!(subtract(4, 2), 2)
    }

    #[test]
    fn test_is_ascii() {
        assert!(is_ascii('a'))
    }
}
