fn create_arr(a: char, b: char, c: char) -> [char; 3] {
    [a, b, c]
}

fn get_slice(arr: &[usize; 5]) -> &[usize] {
    &arr[0..2]
}

fn create_tuple(a: char, b: char, c: char) -> (char, char, char) {
    (a, b, c)
}

fn reverse(pair: (i32, bool)) -> (bool, i32) {
    let (int_param, bool_param) = pair;
    (bool_param, int_param)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_create_arr() {
        assert_eq!(create_arr('a', 'b', 'c'), ['a', 'b', 'c']);
    }

    #[test]
    fn test_use_slice() {
        let array: [usize; 5] = core::array::from_fn(|i| i + 1);
        assert_eq!(get_slice(&array), [1, 2])
    }

    #[test]
    fn test_create_tuple() {
        assert_eq!(create_tuple('a', 'b', 'c'), ('a', 'b', 'c'))
    }

    #[test]
    fn test_reverse() {
        assert_eq!(reverse((1, true)), (true, 1))
    }
}
