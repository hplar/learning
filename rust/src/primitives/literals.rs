fn integer_addition(a: u32, b: u32) -> String {
    format!("{a} + {b} = {}", a + b)
}

fn integer_subtraction(a: i32, b: i32) -> String {
    format!("{a} - {b} = {}", a - b)
}

fn short_circuit_boolean_and(a: bool, b: bool) -> String {
    format!("{a} AND {b} is {}", a && b)
}

fn short_circuit_boolean_or(a: bool, b: bool) -> String {
    format!("{a} OR {b} is {}", a || b)
}

fn short_circuit_boolean_not(a: bool) -> String {
    format!("NOT {a} is {}", !a)
}

fn bitwise_operation(a: u32, b: u32, c: u32) -> String {
    format!("{a} AND {b} = {c}")
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_integer_addition() {
        assert_eq!(integer_addition(1, 2), "1 + 2 = 3")
    }

    #[test]
    fn test_integer_subtraction() {
        assert_eq!(integer_subtraction(4, 2), "4 - 2 = 2")
    }

    #[test]
    fn test_short_circuit_boolean_and() {
        assert_eq!(
            short_circuit_boolean_and(true, false),
            "true AND false is false"
        )
    }

    #[test]
    fn test_short_circuit_boolean_or() {
        assert_eq!(
            short_circuit_boolean_or(true, false),
            "true OR false is true"
        )
    }

    #[test]
    fn test_short_circuit_boolean_not() {
        assert_eq!(short_circuit_boolean_not(true), "NOT true is false")
    }

    #[test]
    fn test_bitwise_operation() {
        assert_eq!(bitwise_operation(0b0011, 0b0101, 0b1000), "3 AND 5 = 8")
    }
}
