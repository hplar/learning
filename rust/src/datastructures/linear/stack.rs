struct Stack {
    items: Vec<u32>,
}

impl Stack {
    pub fn push(&mut self, val: u32) {
        self.items.push(val);
    }

    pub fn pop(&mut self) -> u32 {
        self.items.pop().unwrap()
    }

    pub fn peek(&self) -> &u32 {
        self.items.last().unwrap()
    }

    pub fn is_empty(&self) -> bool {
        self.items.is_empty()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_push() {
        let mut stack = Stack { items: Vec::new() };
        stack.push(42);
        assert_eq!(stack.items.len(), 1);
    }

    #[test]
    pub fn test_pop() {
        let mut stack = Stack { items: Vec::new() };
        stack.push(420);
        assert_eq!(stack.pop(), 420);
    }

    #[test]
    #[should_panic]
    pub fn test_pop_empty_stack() {
        let mut stack = Stack { items: Vec::new() };
        stack.pop();
    }

    #[test]
    pub fn test_peek() {
        let mut stack = Stack { items: Vec::new() };
        stack.push(69);
        stack.push(1337);
        assert_eq!(stack.peek(), &1337);
    }

    #[test]
    #[should_panic]
    pub fn test_peek_empty_stack() {
        let stack = Stack { items: Vec::new() };
        stack.peek();
    }

    #[test]
    pub fn test_is_empty_ok() {
        let stack = Stack { items: Vec::new() };
        assert!(stack.is_empty());
    }

    #[test]
    pub fn test_is_empty_nok() {
        let mut stack = Stack { items: Vec::new() };
        stack.push(1);
        assert!(!stack.is_empty());
    }
}
