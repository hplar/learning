#![allow(dead_code)] // not using any code outside of test functions.

mod algorithms;
mod datastructures;
mod primitives;
