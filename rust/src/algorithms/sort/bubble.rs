fn bubble_sort<T: Ord>(arr: &mut [T]) {
    for i in 0..arr.len() {
        for j in 0..arr.len() - 1 - i {
            if arr[j] > arr[j + 1] {
                arr.swap(j, j + 1);
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_i32() {
        let mut arr: [i32; 8] = [8, 2, 10, 14, 4, -31, 12, 1];
        bubble_sort(&mut arr);

        assert_eq!(arr, [-31, 1, 2, 4, 8, 10, 12, 14])
    }

    #[test]
    fn test_strings() {
        let mut arr: [String; 5] = [
            "zinger".to_string(),
            "alphabet".to_string(),
            "errata".to_string(),
            "abstract".to_string(),
            "trigonometry".to_string(),
        ];
        bubble_sort(&mut arr);

        assert_eq!(
            arr,
            [
                "abstract".to_string(),
                "alphabet".to_string(),
                "errata".to_string(),
                "trigonometry".to_string(),
                "zinger".to_string(),
            ]
        )
    }
}
