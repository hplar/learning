fn insertion_sort<T: Ord>(arr: &mut [T]) {
    for i in 1..arr.len() {
        let mut j = i;
        while j > 0 && arr[j] < arr[j - 1] {
            arr.swap(j, j - 1);
            j = j - 1;
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_i32() {
        let mut arr: [i32; 4] = [24, 12, 36, 1];
        insertion_sort(&mut arr);
        assert_eq!(arr, [1, 12, 24, 36]);
    }

    #[test]
    fn test_strings() {
        let mut arr: [String; 5] = [
            "zinger".to_string(),
            "alphabet".to_string(),
            "errata".to_string(),
            "abstract".to_string(),
            "trigonometry".to_string(),
        ];
        insertion_sort(&mut arr);

        assert_eq!(
            arr,
            [
                "abstract".to_string(),
                "alphabet".to_string(),
                "errata".to_string(),
                "trigonometry".to_string(),
                "zinger".to_string(),
            ]
        )
    }
}
