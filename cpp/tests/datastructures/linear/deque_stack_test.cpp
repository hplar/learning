#include <gtest/gtest.h>
#include <linear/deque_stack.h>

TEST(DequeStackTest, TestSizeEmptyStack) {
    auto s = DequeStack<int>();
    ASSERT_EQ(0, s.getSize());
}

TEST(DequeStackTest, TestPush) {
    auto s = DequeStack<int>();
    for (int i = 0; i < 5; i++) {
        s.push(i);
    }
    ASSERT_EQ(5, s.getSize());
}

TEST(DequeStackTest, TestPop) {
    auto s = DequeStack<int>();
    for (int i = 0; i < 5; i++) {
        s.push(i);
    }
    for (int i = 4; i >= 0; i--) {
        ASSERT_EQ(i, s.pop());
        ASSERT_EQ(i, s.getSize());
    }
}

TEST(DequeStackTest, TestPopEmptyStack) {
    auto s = DequeStack<int>();
    ASSERT_THROW(s.pop(), std::runtime_error);
}

TEST(DequeStackTest, TestPeek) {
    auto s = DequeStack<int>();
    for (int i = 0; i < 5; i++) {
        s.push(i);
    }
    ASSERT_EQ(4, s.peek());
}

TEST(DequeStackTest, TestPeekEmptyStack) {
    auto s = DequeStack<int>();
    ASSERT_THROW(s.peek(), std::runtime_error);
}
