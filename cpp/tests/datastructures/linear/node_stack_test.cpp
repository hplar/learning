#include <gtest/gtest.h>
#include <linear/node_stack.h>
#include <vector>

using namespace std;

TEST(NodeStackTest, TestSize) {
    NodeStack s = NodeStack<int>();
    ASSERT_EQ(0, s.getSize());
}

TEST(NodeStackTest, TestPush) {
    NodeStack s = NodeStack<int>();
    for (int i = 0; i < 5; i++) {
        s.push(i);
    }
    ASSERT_EQ(5, s.getSize());
}

TEST(NodeStackTest, TestPop) {
    NodeStack s = NodeStack<int>();
    for (int i = 0; i < 5; i++) {
        s.push(i);
    }
    for (int i = 4; i >= 0; i--) {
        ASSERT_EQ(i, s.pop());
    }
}

TEST(NodeStackTest, TestPopEmpyStack) {
    NodeStack s = NodeStack<int>();
    ASSERT_THROW(s.pop(), std::runtime_error);
}

TEST(NodeStackTest, TestPeek) {
    NodeStack s = NodeStack<int>();
    for (int i = 0; i < 5; i++) {
        s.push(i);
    }
    ASSERT_EQ(4, s.peek());
}

TEST(NodeStackTest, TestPeekEmpyStack) {
    NodeStack s = NodeStack<int>();
    ASSERT_THROW(s.peek(), std::runtime_error);
}


TEST(NodeStackTest, TestPushStrings) {
    auto vs = NodeStack<string>();
    vector<string> strings {"nice weather", "getting beter", "birds of a feather"};

    for (string s : strings) {
        vs.push(s);
    }

    ASSERT_EQ(3, vs.getSize());
}

TEST(NodeStackTest, TestPushFloats) {
    auto s = NodeStack<float>();
    vector<float> floats {6.9f, 4.20f, 1.87f};


    for (float f : floats) {
        s.push(f);
    }

    ASSERT_EQ(3, s.getSize());
}

TEST(NodeStackTest, TestPushDoubles) {
    auto s = NodeStack<double>();
    vector<double> doubles{6.9, 4.20, 1.87};


    for (double d: doubles) {
        s.push(d);
    }

    ASSERT_EQ(3, s.getSize());
}
