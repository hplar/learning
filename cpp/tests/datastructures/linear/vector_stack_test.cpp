#include <gtest/gtest.h>
#include <linear/vector_stack.h>
#include <vector>

using namespace std;

TEST(VectorStackTest, TestSize) {
    auto s = VectorStack<int>();
    ASSERT_EQ(0, s.getSize());
}

TEST(VectorStackTest, TestPush) {
    auto s = VectorStack<int>();
    for (int i = 0; i < 5; i++) {
        s.push(i);
    }
    ASSERT_EQ(5, s.getSize());
}

TEST(VectorStackTest, TestPop) {
    auto s = VectorStack<int>();
    for (int i = 0; i < 5; i++) {
        s.push(i);
    }
    for (int i = 4; i >= 0; i--) {
        ASSERT_EQ(i, s.pop());
    }
}

TEST(VectorStackTest, TestPopEmptyStack) {
    auto s = VectorStack<int>();
    ASSERT_THROW(s.pop(), std::runtime_error);
}

TEST(VectorStackTest, TestPeek) {
    auto s = VectorStack<int>();
    for (int i = 0; i < 5; i++) {
        s.push(i);
    }
    ASSERT_EQ(4, s.peek());
}

TEST(VectorStackTest, TestPeekEmptyStack) {
    auto s = VectorStack<int>();
    ASSERT_THROW(s.peek(), std::runtime_error);
}

TEST(VectorStackTest, TestPushStrings) {
    auto vs = VectorStack<string>();
    vector<string> strings {"nice weather", "getting beter", "birds of a feather"};

    for (string s : strings) {
        vs.push(s);
    }

    ASSERT_EQ(3, vs.getSize());
}

TEST(VectorStackTest, TestPushFloats) {
    auto s = VectorStack<float>();
    vector<float> floats {6.9f, 4.20f, 1.87f};


    for (float f : floats) {
        s.push(f);
    }

    ASSERT_EQ(3, s.getSize());
}

TEST(VectorStackTest, TestPushDoubles) {
    auto s = VectorStack<double>();
    vector<double> doubles{6.9, 4.20, 1.87};


    for (double d: doubles) {
        s.push(d);
    }

    ASSERT_EQ(3, s.getSize());
}
