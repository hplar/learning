#include <gtest/gtest.h>
#include <linear/linkedlist.h>
#include <vector>

TEST(LinkedListTest, TestIsEmpty) {
    LinkedList l = LinkedList<int>();
    ASSERT_TRUE(l.isEmpty());
}

TEST(LinkedListTest, TestPush) {
    LinkedList l = LinkedList<int>();
    for (int i = 0; i < 5; i++) {
        l.push(i);
        ASSERT_EQ(i, l.peekLast());
    }
}

TEST(LinkedListTest, TestSearchOK) {
    LinkedList l = LinkedList<int>();
    for (int i = 0; i < 5; i++) {
        l.push(i);
    }
    ASSERT_EQ(3, l.search(3));
}

TEST(LinkedListTest, TestSearchNOK) {
    LinkedList l = LinkedList<int>();
    l.push(0);

    ASSERT_THROW(l.search(100), std::runtime_error);
}

TEST(LinkedListTest, TestSearchEmptyList) {
    LinkedList l = LinkedList<int>();
    ASSERT_THROW(l.search(3), std::runtime_error);
}

TEST(LinkedListTest, TestPop) {
    LinkedList l = LinkedList<int>();
    for (int i = 0; i < 5; i++) {
        l.push(i);
    }
    ASSERT_EQ(5, l.getSize());

    l.pop(2);
    ASSERT_EQ(4, l.getSize());

    l.pop(0);
    ASSERT_EQ(3, l.getSize());

    l.pop(2);
    ASSERT_EQ(2, l.getSize());
    ASSERT_EQ(1, l.peekFirst());
    ASSERT_EQ(3, l.peekLast());
}

TEST(LinkedListTest, TestPopOneNode) {
    LinkedList l = LinkedList<int>();
    l.push(0);
    l.pop(0);
    ASSERT_TRUE(l.isEmpty());
}

TEST(LinkedListTest, TestPopEmptyList) {
    LinkedList l = LinkedList<int>();
    ASSERT_THROW(l.pop(1), std::runtime_error);
}

TEST(LinkedListTest, TestPopNegativeIndex) {
    LinkedList l = LinkedList<int>();
    l.push(1);
    ASSERT_THROW(l.pop(-1), std::out_of_range);
}

TEST(LinkedListTest, TestPopIndexOutOfRange) {
    LinkedList l = LinkedList<int>();
    l.push(1);
    ASSERT_THROW(l.pop(2), std::out_of_range);
}

TEST(LinkedListTest, TestPeekFirst) {
    LinkedList l = LinkedList<int>();
    l.push(0);
    l.push(1);

    ASSERT_EQ(0, l.peekFirst());
}

TEST(LinkedListTest, TestPeekFirstEmptyList) {
    LinkedList l = LinkedList<int>();
    ASSERT_THROW(l.peekFirst(), std::runtime_error);
}

TEST(LinkedListTest, TestPeekLast) {
    LinkedList l = LinkedList<int>();
    for (int i = 0; i < 5; i++) {
        l.push(i);
    }
    ASSERT_EQ(4, l.peekLast());
}

TEST(LinkedListTest, TestPeekLastEmptyList) {
    LinkedList l = LinkedList<int>();
    ASSERT_THROW(l.peekLast(), std::runtime_error);
}

TEST(LinkedListTest, TestGetSize) {
    LinkedList l = LinkedList<int>();
    for (int i = 0; i < 25; i++) {
        l.push(i);
    }
    ASSERT_EQ(25, l.getSize());
}

TEST(LinkedListTest, TestGetSizeEmptyList) {
    LinkedList l = LinkedList<int>();
    ASSERT_EQ(0, l.getSize());
}

TEST(LinkedListTest, TestPushString) {
    LinkedList l = LinkedList<std::string>();
    std::vector<std::string> strings;
    strings.emplace_back("getting better");
    strings.emplace_back("nice weather");
    strings.emplace_back("birds of a feather");

    for (std::string s: strings) {
        l.push(s);
        ASSERT_EQ(s, l.peekLast());
    }

    ASSERT_EQ(strings.size(), l.getSize());
}

TEST(LinkedListTest, TestPushDouble) {
    LinkedList l = LinkedList<double>();
    std::vector<double> doubles;
    doubles.emplace_back(6.9);
    doubles.emplace_back(4.20);

    for (double d: doubles) {
        l.push(d);
        ASSERT_EQ(d, l.peekLast());
    }

    ASSERT_EQ(doubles.size(), l.getSize());
}

TEST(LinkedListTest, TestPushFloat) {
    LinkedList l = LinkedList<float>();
    std::vector<float> floats;
    floats.emplace_back(6.9f);
    floats.emplace_back(4.20f);

    for (float d: floats) {
        l.push(d);
        ASSERT_EQ(d, l.peekLast());
    }

    ASSERT_EQ(floats.size(), l.getSize());
}

TEST(LinkedListTest, TestPushLinkedList) {
    LinkedList l = LinkedList<LinkedList<int>>();
    std::vector<LinkedList<int>> linkedlists;

    for (int i = 0; i < 3; i++) {
        linkedlists.emplace_back(LinkedList<int>());
    }

    for (LinkedList<int> ll : linkedlists) {
        l.push(ll);
    }

    ASSERT_EQ(linkedlists.size(), l.getSize());
}
