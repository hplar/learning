#include <gtest/gtest.h>
#include <trees/binary_search_tree.h>

using namespace std;

TEST(BinarySearchTreeTest, TestEmptyTree) {
    auto bst = BinarySearchTree<int>();
    ASSERT_EQ(bst.getSize(), 0);
}

TEST(BinarySearchTreeTest, TestAdd) {
    auto bst = BinarySearchTree<int>();

    auto *root = new BSTNode<int>();
    root->obj = 6;
    auto *n1 = new BSTNode<int>();
    n1->obj = 9;
    auto *n2 = new BSTNode<int>();
    n2->obj = 1;
    auto *n3 = new BSTNode<int>();
    n3->obj = 12;
    auto *n4 = new BSTNode<int>();
    n4->obj = 4;

    bst.insert(root);
    bst.insert(n1);
    bst.insert(n2);
    bst.insert(n3);
    bst.insert(n4);

    ASSERT_EQ(bst.getSize(), 5);
}

TEST(BinarySearchTreeTest, TestRemove) {
    BinarySearchTree bst = BinarySearchTree<int>();

    auto *root = new BSTNode<int>();
    root->obj = 6;
    auto *n1 = new BSTNode<int>();
    n1->obj = 9;
    auto *n2 = new BSTNode<int>();
    n2->obj = 1;
    auto *n3 = new BSTNode<int>();
    n2->obj = 10;

    bst.insert(root);
    bst.insert(n1);
    bst.insert(n2);
    bst.insert(n3);
    ASSERT_EQ(bst.getSize(), 4);

    bst.remove(n1);
    ASSERT_EQ(bst.getSize(), 3);

    bst.remove(root);
    ASSERT_EQ(bst.getSize(), 2);

    bst.remove(n2);
    ASSERT_EQ(bst.getSize(), 1);

    bst.remove(n3);
    ASSERT_EQ(bst.getSize(), 0);
}

TEST(BinarySearchTreeTest, TestRemoveLeftSideNode) {
    BinarySearchTree bst = BinarySearchTree<int>();

    auto *root = new BSTNode<int>();
    root->obj = 2;
    auto *n1 = new BSTNode<int>();
    n1->obj = 1;

    bst.insert(root);
    bst.insert(n1);

    bst.remove(n1);
    ASSERT_EQ(bst.getSize(), 1);
}

TEST(BinarySearchTreeTest, TestRemoveOnlyRoot) {
    BinarySearchTree bst = BinarySearchTree<int>();

    auto *root = new BSTNode<int>();
    root->obj = 6;
    bst.insert(root);
    ASSERT_EQ(bst.getSize(), 1);

    bst.remove(root);
    ASSERT_EQ(bst.getSize(), 0);
}

TEST(BinarySearchTreeTest, TestRemoveEmptyTree) {
    BinarySearchTree bst = BinarySearchTree<int>();
    auto *n = new BSTNode<int>();
    ASSERT_THROW(bst.remove(n), std::runtime_error);
}

TEST(BinarySearchTreeTest, TestTraverse) {
    BinarySearchTree bst = BinarySearchTree<int>();
    vector<int> ints {6, 9 , 1, 10};

    for (int i : ints) {
        auto *node = new BSTNode<int>();
        node->obj = i;
        bst.insert(node);
        ASSERT_EQ(bst.find(i)->obj, i);
    }
}

TEST(BinarySearchTreeTest, TestTraverseNodeNotInTree) {
    BinarySearchTree bst = BinarySearchTree<int>();

    auto *root = new BSTNode<int>();
    root->obj = 6;

    bst.insert(root);

    ASSERT_THROW(bst.find(10), std::runtime_error);
}

TEST(BinarySearchTreeTest, TestTraverseEmptyTree) {
    BinarySearchTree bst = BinarySearchTree<int>();
    ASSERT_THROW(bst.find(0), std::runtime_error);
}

TEST(BinarySearchTreeTest, TestAddDoubles) {
    auto bst = BinarySearchTree<double>();
    vector<double> doubles {6.9, 4.20, 18.7};

    for (double d : doubles) {
        auto *node = new BSTNode<double>();
        node->obj = d;
        bst.insert(node);
    }

    ASSERT_EQ(bst.getSize(), 3);
}

TEST(BinarySearchTreeTest, TestAddStrings) {
    auto bst = BinarySearchTree<string>();
    vector<string> strings {"nice weather", "getting better", "birds of a feather"};

    for (const string& str : strings) {
        auto *node = new BSTNode<string>();
        node->obj = str;
        bst.insert(node);
    }

    ASSERT_EQ(bst.getSize(), 3);
}

TEST(BinarySearchTreeTest, TestAddFloats) {
    auto bst = BinarySearchTree<float>();
    vector<float> floats {6.9f, 4.20f, 18.7f};

    for (float f : floats) {
        auto *node = new BSTNode<float>();
        node->obj = f;
        bst.insert(node);
    }

    ASSERT_EQ(bst.getSize(), 3);
}
