#ifndef LEARNING_BINARY_SEARCH_TREE_H
#define LEARNING_BINARY_SEARCH_TREE_H

#include <stack>
#include <stdexcept>

template <class T>
struct BSTNode {
    T obj;
    BSTNode<T> *parent;
    BSTNode<T> *left;
    BSTNode<T> *right;
};

template <class T>
class BinarySearchTree {

private:
    int size = 0;
protected:
    BSTNode<T> *root = nullptr;

public:
    virtual ~BinarySearchTree() = default;

    BSTNode<T> insert(BSTNode<T> *node, BSTNode<T> *start_node = nullptr, bool incrementSize = true) {
        if (root == nullptr) {
            root = node;
            if (incrementSize) {
                size += 1;
            }
            return *root;
        }

        if (start_node == nullptr) {
            start_node = root;
        }

        while (start_node != nullptr) {
            if (node->obj < start_node->obj) {
                if (start_node->left == nullptr) {
                    start_node->left = node;
                    start_node->left->parent = start_node;
                    break;
                }
                start_node = start_node->left;
            }

            if (node->obj > start_node->obj) {
                if (start_node->right == nullptr) {
                    start_node->right = node;
                    start_node->right->parent = start_node;
                    break;
                }
                start_node = start_node->right;
            }
        }

        if (incrementSize) {
            size += 1;
        }
        return *node;
    }

    BSTNode<T> remove(BSTNode<T> *node) {
        if (root == nullptr) {
            throw std::runtime_error("Tree is empty");
        }

        if (node->parent == nullptr) {
            root = nullptr;
        } else if (node->parent->left == node) {
            node->parent->left = nullptr;
        } else if (node->parent->right == node) {
            node->parent->right = nullptr;
        }

        if (node->left != nullptr) {
            insert(node->left, node->parent, false);
        }

        if (node->right != nullptr) {
            insert(node->right, node->parent, false);
        }

        node->parent = nullptr;
        node->left = nullptr;
        node->right = nullptr;

        size -= 1;
        return *node;
    }

    [[nodiscard]] int getSize() const {
        return size;
    }

    virtual BSTNode<T> * find(T obj) {
        if (root == nullptr) {
            throw std::runtime_error("Tree is empty");
        }

        auto curr = root;

        while (curr != nullptr) {
            if (obj == curr->obj) {
                return curr;
            }
            if (obj < curr->obj) {
                curr = curr->left;
                continue;
            }
            curr = curr->right;
        }
        throw std::runtime_error("Value not in tree");
    };

    void clear() {
        delete root;
    };

    bool isEmpty() {};

};

#endif //LEARNING_BINARY_SEARCH_TREE_H
