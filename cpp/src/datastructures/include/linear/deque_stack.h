#ifndef LEARNING_DEQUE_STACK_H
#define LEARNING_DEQUE_STACK_H

#include <linear/stack.h>
#include <stdexcept>
#include <queue>


template <class T>
class DequeStack : public Stack<T> {

private:
    std::deque<T> q;

public:
    DequeStack() = default;

    void push(T obj) {
        q.push_back(obj);
    }

    T pop() {
        if (q.empty()) {
            throw std::runtime_error("Stack is empty");
        }

        int val = q.back();
        q.pop_back();
        return val;
    }

    T peek() {
        if (q.empty()) {
            throw std::runtime_error("Stack is empty");
        }

        return q.back();
    }

    int getSize() {
        return q.size();
    }

};

#endif
