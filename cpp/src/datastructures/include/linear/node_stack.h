#ifndef LEARNING_NODE_STACK_H
#define LEARNING_NODE_STACK_H

#include <linear/stack.h>
#include <stdexcept>

template <class T>
struct StackNode {
    T obj;
    StackNode<T> *next;
};

template <class T>
class NodeStack : public Stack<T> {

private:
    StackNode<T> *root;
    int size;

public:

    void push(T obj) override {
        auto *n = new StackNode<T>();
        n->obj = obj;

        if (root == nullptr) {
            root = n;
            size += 1;
            return;
        }

        StackNode<T> *curr = root;
        while (curr->next != nullptr) {
            curr = curr->next;
        }

        curr->next = n;
        size += 1;
    }

    T pop() override {
        if (root == nullptr) {
            throw std::runtime_error("Stack is empty");
        }

        if (size == 1) {
            T obj = root->obj;
            root = nullptr;
            size = 0;
            return obj;
        }

        StackNode<T> *curr = root;
        StackNode<T> *prev = nullptr;

        while (curr->next != nullptr) {
            prev = curr;
            curr = curr->next;
        }

        T obj = curr->obj;
        prev->next = nullptr;

        size -= 1;
        return obj;
    }

    T peek() override {
        if (root == nullptr) {
            throw std::runtime_error("Stack is empty");
        }

        StackNode<T> *curr = root;
        while (curr->next != nullptr) {
            curr = curr->next;
        }

        return curr->obj;
    }

    int getSize() override {
        return size;
    }

};

#endif //LEARNING_NODE_STACK_H
