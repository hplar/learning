#ifndef LEARNING_STACKVECTOR_H
#define LEARNING_STACKVECTOR_H

#include <linear/stack.h>
#include <stdexcept>
#include <vector>

template<class T>
class VectorStack : public Stack<T> {

private:
    std::vector<T> vec;

public:
    void push(T obj) override {
        vec.push_back(obj);
    }

    T pop() override {
        if (vec.empty()) {
            throw std::runtime_error("Stack is empty");
        }

        T obj = vec.at(vec.size() - 1);
        vec.pop_back();
        return obj;
    }

    T peek() override {
        if (vec.empty()) {
            throw std::runtime_error("Stack is empty");
        }

        return vec.at(vec.size() - 1);
    }

    int getSize() override {
        return vec.size();
    }

};

#endif
