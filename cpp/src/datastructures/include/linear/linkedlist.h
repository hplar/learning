#ifndef LEARNING_LINKEDLIST_H
#define LEARNING_LINKEDLIST_H

using namespace std;

template <class T>
struct LinkedListNode {
    T obj;
    LinkedListNode<T> *next = nullptr;
};

template <class T>
class LinkedList {
    LinkedListNode<T> *root = nullptr;
    int size = 0;

public:
    LinkedList() = default;;

    void push(T obj) {
        auto *n = new LinkedListNode<T>();
        n->obj = obj;
        n->next = nullptr;

        LinkedListNode<T> *curr = root;

        if (curr == nullptr) {
            root = n;
            size += 1;
            return;
        }

        while (curr->next != nullptr) {
            curr = curr->next;
        }

        curr->next = n;

        size += 1;
    }

    T search(T obj) {
        if (isEmpty()) {
            throw std::runtime_error("LinkedList is empty");
        }

        auto curr = root;

        while (curr != nullptr) {
            if (curr->obj == obj) {
                return curr->obj;
            }

            curr = curr->next;
        }

        throw std::runtime_error("Value not found");
    }

    void pop(int index) {
        if (isEmpty()) {
            throw std::runtime_error("LinkedList is empty");
        }

        if (index < 0 || index >= size) {
            throw std::out_of_range("Out of range");
        }

        if (index == 0 && root->next == nullptr) {
            root = nullptr;
            size -= 1;
            return;
        }

        if (index == 0 and root->next != nullptr) {
            root = root->next;
            size -= 1;
            return;
        }

        LinkedListNode<T> *prev;
        LinkedListNode<T> *curr = root;

        for (int i = 0; i < index; i++) {
            prev = curr;
            curr = curr->next;
        }

        if (curr->next != nullptr) {
            prev->next = curr->next;
        } else {
            prev->next = nullptr;
        }

        size -= 1;
    }

    T peekFirst() {
        if (root == nullptr) {
            throw std::runtime_error("LinkedList is empty");
        }

        return root->obj;
    }

    T peekLast() {
        if (root == nullptr) {
            throw std::runtime_error("LinkedList is empty");
        }

        LinkedListNode<T> *curr = root;
        while (curr->next != nullptr) {
            curr = curr->next;
        }

        return curr->obj;
    }

    int getSize() {
        return size;
    }

    bool isEmpty() {
        return root == nullptr;
    }

};

#endif //LEARNING_LINKEDLIST_H
