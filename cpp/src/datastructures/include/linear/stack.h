#ifndef LEARNING_STACK_H
#define LEARNING_STACK_H


template <class T>
class Stack {
public:
    virtual ~Stack() = default;;
    virtual void push(T obj) = 0;
    virtual T pop() = 0;
    virtual T peek() = 0;
    virtual int getSize() = 0;
};

#endif
