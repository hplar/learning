package com.learning.Datastructures.Trees;

public class BinarySearchTree {

    private Node root;

    public BinarySearchTree() {
    }

    public Node getRoot() {
        return this.root;
    }

    public Node insert(Integer value) {
        return insert(new Node(value), this.root);
    }

    public Node insert(Node newNode, Node currentNode) {
        if (currentNode == null) {
            this.root = newNode;
            return this.root;
        }

        if (newNode.value < currentNode.value) {
            if (currentNode.getLeft() == null) {
                currentNode.setLeft(newNode);
                return newNode;
            }

            insert(newNode, currentNode.getLeft());

        }

        if (newNode.value > currentNode.value) {
            if (currentNode.getRight() == null) {
                currentNode.setRight(newNode);
                return newNode;
            }

            insert(newNode, currentNode.getRight());
        }

        return newNode;
    }

    public Node search(Integer value) {
        if (this.getRoot() == null) {
            return null;
        }

        var currentNode = root;

        while (currentNode != null) {
            if (value.equals(currentNode.getValue())) {
                return currentNode;
            }

            if (value < currentNode.getValue()) {
                currentNode = currentNode.getLeft();
                continue;
            }

            currentNode = currentNode.getRight();
        }

        return null;
    }

    public Node searchParent(Integer value) {
        if (getRoot() == null) {
            return null;
        }

        var currentNode = root;
        Node prevNode = null;

        while (currentNode != null) {
            if (currentNode.getValue().equals(value)) {
                return prevNode;
            }

            prevNode = currentNode;

            if (value < currentNode.getValue()) {
                currentNode = currentNode.getLeft();
                continue;
            }

            currentNode = currentNode.getRight();
        }

        return null;
    }

    public Node remove(Integer value) {
        if (getRoot() == null) {
            return null;
        }

        if (value.equals(getRoot().getValue())) {
            var ret = getRoot();
            root = null;
            return ret;
        }

        var parent = searchParent(value);

        if (parent == null) {
            return null;
        }

        Node removedNode = null;

        if (value < parent.getValue()) {
            removedNode = parent.getLeft();
            parent.setLeft(null);
            insert(removedNode.getLeft(), getRoot());
        }

        if (value > parent.getValue()) {
            removedNode = parent.getRight();
            parent.setRight(null);
            insert(removedNode.getRight(), getRoot());
        }

        return removedNode;
    }

    public static class Node {
        private final Integer value;
        private Node left;
        private Node right;

        public Node(Integer value) {
            this.value = value;
        }

        public Node getLeft() {
            return this.left;
        }

        public void setLeft(Node node) {
            this.left = node;
        }

        public Node getRight() {
            return this.right;
        }

        public void setRight(Node node) {
            this.right = node;
        }

        public Integer getValue() {
            return this.value;
        }
    }
}