package com.learning.Datastructures.HashTables;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HashMap<K, V> {
    public HashMap() {
    }

    private final List<Bucket<V>> buckets = new ArrayList<>();
    private final List<K> keys = new ArrayList<>();

    private static class Bucket<V> {
        private final int hash;
        private V value;

        public Bucket(int hash) {
            this.hash = hash;
        }

        public void set(V value) {
            this.value = value;
        }

        public V getContent() {
            return this.value;
        }

        public boolean matchHash(int hash) {
            return hash == this.hash;
        }

    }

    public void put(K key, V value) {
        for (Bucket<V> bucket : this.buckets) {
            if (bucket.matchHash(key.hashCode())) {
                bucket.set(value);
                return;
            }
        }

        Bucket<V> bucket = new Bucket<>(key.hashCode());
        bucket.set(value);
        this.buckets.add(bucket);
        this.keys.add(key);
    }

    public V get(K key) {
        for (Bucket<V> bucket : this.buckets) {
            if (bucket.matchHash(key.hashCode())) {
                return bucket.getContent();
            }
        }
        throw new RuntimeException("Key does not exist");
    }

    public V remove(K key) {
        for (var i=0; i < this.buckets.size(); i++) {
            if (this.buckets.get(i).matchHash(key.hashCode())) {
                var ret = this.buckets.get(i).getContent();
                this.buckets.remove(i);
                this.keys.remove(i);
                return ret;
            }
        }

        return null;
    }

    public Set<K> keySet() {
        return new HashSet<>(this.keys);
    }

    public int size() {
        return this.buckets.size();
    }
}