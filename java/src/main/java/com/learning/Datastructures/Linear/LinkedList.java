package com.learning.Datastructures.Linear;

public class LinkedList<T> {

    private Node<T> root;

    public LinkedList() {
    }

    public Node<T> add(T value) {
        var node = new Node<>(value);

        if (getRoot() == null) {
            root = node;
            return node;
        }

        return add(node, root);
    }

    public Node<T> add(Node<T> newNode, Node<T> currentNode) {
        if (currentNode.getNext() == null) {
            currentNode.setNext(newNode);
            return newNode;
        } else {
            return add(newNode, currentNode.getNext());
        }
    }

    public Node<T> search(T value) {
        if (getRoot() == null) {
            return null;
        }

        return search(value, root);
    }

    public Node<T> search(T value, Node currentNode) {
        if (currentNode.getValue().equals(value)) {
            return currentNode;
        }

        if (currentNode.getNext() != null) {
            return search(value, currentNode.getNext());
        }

        return null;
    }

    public Node<T> remove(T value) {
        var currentNode = root;
        Node<T> tmpNode;

        if (getRoot().getValue().equals(value)) {
            tmpNode = root;
            setRoot(root.getNext());
            return tmpNode;
        }

        while (currentNode.getNext() != null) {
            if (currentNode.getNext().getValue() == value) {
                tmpNode = currentNode.getNext();
                currentNode.setNext(null);
                add(tmpNode.getNext(), root);
                return tmpNode;
            }

            currentNode = currentNode.getNext();
        }

        return null;
    }

    public Node<T> setRoot(Node<T> node) {
        this.root = node;
        return root;
    }

    public Node<T> getRoot() {
        return root;
    }

    public Integer size() {
        var size = 0;
        var currentNode = root;

        while (currentNode != null) {
            currentNode = currentNode.getNext();
            size += 1;
        }

        return size;
    }

    public static class Node<T> {
        public T value;
        private Node<T> next;

        public Node(T value) {
            this.value = value;
        }

        public T getValue() {
            return value;
        }

        public Node<T> getNext() {
            return next;
        }

        public void setNext(Node<T> next) {
            this.next = next;
        }
    }

}