package com.learning.Datastructures.Linear;

import java.util.EmptyStackException;

public class Stack<T> {

    private Node<T> top;

    public void push(T val) {
        var newTop = new Node<>(val);

        if (top == null) {
            top = newTop;
            return;
        }

        newTop.setPrev(top);
        top = newTop;
    }

    public T pop() {
        if (top == null) {
            throw new EmptyStackException();
        }

        var tmp = top;
        top = top.getPrev();

        return tmp.getValue();
    }

    public T peek() {
        if (top == null) {
            throw new EmptyStackException();
        }

        return top.getValue();
    }

    public boolean isEmpty() {
        return top == null;
    }

    public int size() {
        if (top == null) {
            return 0;
        }

        var count = 0;
        var current = top;

        while (current != null) {
            count += 1;
            current = current.getPrev();
        }

        return count;
    }


    private static class Node<T> {

        private final T value;
        private Node<T> prev;

        public Node(T value) {
            this.value = value;
        }

        public void setPrev(Node<T> next) {
            this.prev = next;
        }

        public Node<T> getPrev() {
            return this.prev;
        }

        public T getValue() {
            return this.value;
        }
    }
}