package com.learning.Datastructures.LanguageFeatures.Streams;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

/**
 * Experiments with Java Streams
 */
final public class IntegerStream {

    public static List<Integer> getEvenSubset(Collection<Integer> ints) {
        return ints.stream().filter(n -> n % 2 == 0).toList();
    }

    public static List<Integer> getUnevenSubset(Collection<Integer> ints) {
        var uneven = new Uneven();
        return ints.stream().filter(uneven).toList();
    }

    public static Integer getSum(Collection<Integer> ints) {
        return ints.stream().reduce(0, Math::addExact);
    }

    public static Integer getProduct(Collection<Integer> ints) {
        return ints.stream().reduce(1, Math::multiplyExact);
    }

    public static Integer getDifference(Collection<Integer> ints) {
        return ints.stream().reduce(0, Math::subtractExact);
    }

    private static class Uneven implements Predicate<Integer> {

        @Override
        public boolean test(Integer i) {
            return i % 2 != 0;
        }
    }
}

