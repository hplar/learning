package com.learning.Datastructures.Trees;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class BinarySearchTreeTest {

    @Test
    void testRootNode() {
        var tree = new BinarySearchTree();
        assertNull(tree.getRoot(), "Root node should be null");

        tree.insert(4);
        assertEquals(4, tree.getRoot().getValue(), "Root node should have value 4");
    }

    @Test
    void testInsert() {
        var tree = new BinarySearchTree();

        tree.insert(4);
        assertNull(tree.getRoot().getLeft(), "Left node should be null");
        assertNull(tree.getRoot().getRight(), "Right node should be null");

        tree.insert(2);
        assertEquals(2, tree.getRoot().getLeft().getValue(), "Left node should have value 2");
        assertNull(tree.getRoot().getRight(), "Right node should be null");

        tree.insert(5);
        assertEquals(2, tree.getRoot().getLeft().getValue(), "Left node should have value 2");
        assertEquals(5, tree.getRoot().getRight().getValue(), "Right node should have value 5");

        var node = tree.insert(10);
        assertEquals(10, node.getValue(), "Node value should be 10");

    }

    @Test
    void testSearch() {
        var tree = new BinarySearchTree();

        for (var value : Arrays.asList(4, 10, 2, 15, 8 ,17, 12)) {
            tree.insert(value);
            assertEquals(value, tree.search(value).getValue());
        }

        assertNull(tree.search(1));
    }

    @Test
    void testSearchEmptyTree() {
        var tree = new BinarySearchTree();
        assertNull(tree.search(1), "Should return null");
    }

    @Test
    void testSearchNonExistingValue() {
        var tree = new BinarySearchTree();

        for (var value : Arrays.asList(10, 2, 15, 8 ,17, 12)) {
            tree.insert(value);
            assertEquals(value, tree.search(value).getValue());
        }

        assertNull(tree.search(100));
    }

    @Test
    void testSearchParent() {
        var tree = new BinarySearchTree();
        var root = new BinarySearchTree.Node(4);
        var node1 = new BinarySearchTree.Node(2);
        var node2 = new BinarySearchTree.Node(5);
        var node3 = new BinarySearchTree.Node(6);
        var node4 = new BinarySearchTree.Node(1);

        tree.insert(root, null);
        tree.insert(node1, root);
        tree.insert(node2, root);
        tree.insert(node3, root);
        tree.insert(node4, root);

        assertEquals(root, tree.searchParent(2), "Parent node should be root");
        assertEquals(root, tree.searchParent(5), "Parent node should be root");
        assertEquals(node2, tree.searchParent(6), "Parent node should be node2");
        assertEquals(node1, tree.searchParent(1), "Parent node should be node2");
    }

    @Test
    void testSearchParentNonExistingValue() {
        var tree = new BinarySearchTree();
        tree.insert(4);
        tree.insert(5);
        tree.insert(2);

        assertNull(tree.searchParent(6), "Should return null");
    }

    @Test
    void testSearchParentEmptyTree() {
        var tree = new BinarySearchTree();
        assertNull(tree.searchParent(1), "Should return null");
    }

    @Test
    void testRemove() {
        var tree = new BinarySearchTree();
        var root = new BinarySearchTree.Node(4);
        var node1 = new BinarySearchTree.Node(2);
        var node2 = new BinarySearchTree.Node(5);
        var node3 = new BinarySearchTree.Node(6);
        var node4 = new BinarySearchTree.Node(1);

        tree.insert(root, null);
        tree.insert(node1, root);
        tree.insert(node2, root);
        tree.insert(node3, root);
        tree.insert(node4, root);

        assertEquals(node1, tree.remove(2), "Wrong node was removed");
        assertEquals(node2, tree.remove(5), "Wrong node was removed");
        assertNull(tree.search(2), "Node wasn't removed");
        assertEquals(root, tree.searchParent(1), "Node4 parent should be root");
    }

    @Test
    void testRemoveNonExistingNode() {
        var tree = new BinarySearchTree();
        tree.insert(4);
        tree.insert(2);
        tree.insert(5);

        assertNull(tree.remove(10), "Should return null");
    }

    @Test
    void testRemoveRootNode() {
        var tree = new BinarySearchTree();
        var root = tree.insert(4);

        assertEquals(root, tree.remove(4), "Should return null");
    }

    @Test
    void testRemoveEmptyTree() {
        var tree = new BinarySearchTree();
        assertNull(tree.remove(1), "Should return null");
    }
}