package com.learning.Datastructures.Linear;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class LinkedListTest {

    @Test
    void testAdd() {
        var ll = new LinkedList<Integer>();

        for (var i=0; i < 3; i++) {
            ll.add(i);
        }
    }

    @Test
    void testSearch() {
        var ll = new LinkedList<Integer>();
        var node = ll.add(1);
        var node1 = ll.add(2);
        var node2= ll.add(3);

        assertEquals(node, ll.search(1), "Search did not return the correct node");
        assertEquals(node1, ll.search(2), "Search did not return the correct node");
        assertEquals(node2, ll.search(3), "Search did not return the correct node");
    }


    @Test
    void testSearchEmptyList() {
        var ll = new LinkedList<String>();
        assertNull(ll.search("1"), "Should return null");

    }

    @Test
    void testSize() {
        var ll = new LinkedList<Integer>();
        assertEquals(0, ll.size(), "Size should be 0");

        for (var i=0; i < 99; i++) {
            ll.add(i);
        }

        assertEquals(99, ll.size(), "Size should be 99");
    }

    @Test
    void testRemove() {
        var ll = new LinkedList<Integer>();
        var root = ll.add(1);
        var node1 = ll.add(2);
        var node2 = ll.add(3);
        var node3 = ll.add(4);
        var node4 = ll.add(5);

        assertEquals(5, ll.size(), "Size should be 5");
        assertEquals(node1, ll.remove(2), "Remove did not return the correct node");
        assertEquals(4, ll.size(), "Size should be 4");
        assertNull(ll.search(2), "Node with value 2 was not removed");
        assertEquals(node2, root.getNext(), "LinkedList is not reconstructed properly after removal of a node");
        assertEquals(node3, ll.remove(4), "Wrong node removed");
    }

    @Test
    void testRemoveRoot() {
        var ll = new LinkedList<String>();
        var root = ll.add("1");
        assertEquals(root, ll.remove("1"), "Did not return the rootNode");
        assertNull(ll.getRoot(), "Root should be null");
    }

    @Test
    void testRemoveNonExistingValue() {
        var ll = new LinkedList<String>();
        var root = ll.add("1");
        assertNull(ll.remove("3"), "Should return null");
    }

    @Test
    void testGetRoot() {
        var ll = new LinkedList<Integer>();
        var node = ll.add(1);
        assertEquals(node, ll.getRoot(), "getRoot did not return the correct node");
    }

    @Test
    void testSetRoot() {
        var ll = new LinkedList<Integer>();
        var node = ll.add(1);
        assertEquals(node, ll.setRoot(node), "setRoot did not return the correct node");
    }
}