package com.learning.Datastructures.Linear;

import org.junit.jupiter.api.Test;

import java.util.EmptyStackException;

import static org.junit.jupiter.api.Assertions.*;

class StackTest {

    @Test
    void testPushIntegers() {
        var stack = new Stack<Integer>();
        stack.push(1);
        stack.push(2);
        stack.push(3);

        assertEquals(3, stack.pop(), "Pop returned an incorrect value.");
        assertEquals(2, stack.pop(), "Pop returned an incorrect value.");
        assertEquals(1, stack.pop(), "Pop returned an incorrect value.");
    }

    @Test
    void testPushStrings() {
        var stack = new Stack<String>();
        stack.push("1");
        stack.push("2");
        stack.push("3");

        assertEquals("3", stack.pop(), "Pop returned an incorrect value.");
        assertEquals("2", stack.pop(), "Pop returned an incorrect value.");
        assertEquals("1", stack.pop(), "Pop returned an incorrect value.");
    }

    @Test
    void testPeek() {
        var stack = new Stack<Integer>();
        stack.push(1);
        stack.push(2);
        stack.push(3);

        assertEquals(3, stack.peek(), "Peek returned an incorrect value");

        stack.pop();
        assertEquals(2, stack.peek(), "Peek return an incorrect value");
    }

    @Test
    void testIsEmpty() {
        var stack = new Stack<Integer>();
        assertTrue(stack.isEmpty(), "The stack should be empty");
    }

    @Test
    void testIsEmptyStackNotEmpty() {
        var stack = new Stack<Integer>();
        stack.push(1);
        assertFalse(stack.isEmpty(), "The stack should not be empty");
    }

    @Test
    void testSize() {
        var stack = new Stack<Integer>();
        assertEquals(0, stack.size(), "Stack size should be 0");

        for (var i=0; i < 20; i++) {
            stack.push(i);
        }

        assertEquals(20, stack.size(), "Stack size should be 20");
    }

    @Test
    void testPopEmptyStack() {
        var stack = new Stack<Integer>();
        assertThrows(EmptyStackException.class, stack::pop);
    }

    @Test
    void testPeekEmptyStack() {
        var stack = new Stack<Integer>();
        assertThrows(EmptyStackException.class, stack::peek);
    }
}