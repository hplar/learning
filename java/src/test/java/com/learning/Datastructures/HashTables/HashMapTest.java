package com.learning.Datastructures.HashTables;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class HashMapTest {

    @Test
    void testPutStrings() {
        var map = new HashMap<String, String>();
        map.put("key", "value");
        map.put("key2", "value2");
        assertEquals("value", map.get("key"), "put should work");
        assertEquals(2, map.size(), "size should be 2");
    }

    @Test
    void testRemoveStrings() {
        var map = new HashMap<String, String>();
        map.put("key", "value");
        assertEquals(1, map.size(), "size should be 1");

        map.remove("key");
        assertEquals(0, map.size(), "size should be 0");
    }

    @Test
    void testPutIntegers() {
        var map = new HashMap<String, Integer>();
        map.put("key", 1337);
        map.put("key2", 420);
        assertEquals(1337, map.get("key"), "put should work");
        assertEquals(2, map.size(), "size should be 2");
    }

    @Test
    void testRemoveIntegers() {
        var map = new HashMap<String, Integer>();
        map.put("key", 69);
        assertEquals(1, map.size(), "size should be 1");

        assertEquals(69, map.remove("key"), "Return value should be 69");
        assertEquals(0, map.size(), "size should be 0");
    }

    @Test
    void testRemoveDoubles() {
        var map = new HashMap<String, Double>();
        map.put("1", 69.0);
        map.put("2", 1337.0);
        map.put("3", 420.0);

        assertEquals(420, map.remove("3"), "Return value should be 69.0");
    }

    @Test
    void testKeySet() {
        var map = new HashMap<Integer, Double>();
        map.put(1, 0.1);
        map.put(2, 0.2);
        map.put(3, 0.3);

        assertEquals(Set.of(1, 2, 3), map.keySet(), "Wrong keys in set");
    }

    @Test
    void testSize() {
        var map = new HashMap<Integer, Double>();
        map.put(1, 0.1);
        map.put(2, 0.2);
        map.put(3, 0.3);

        assertEquals(3, map.size(), "Wrong size");
    }

    @Test
    void testGetOnNonExistingKey() {
        var map = new HashMap<Integer, String>();
        map.put(1, "1");
        map.put(2, "2");

        assertThrows(RuntimeException.class, () ->  {
            map.get(3);
        });
    }

    @Test
    void testPutOnExistingKey() {
        var map = new HashMap<Integer, String>();
        map.put(1, "1");
        map.put(1, "2");

        assertEquals("2", map.get(1), "Wrong value returned");
    }

    @Test
    void testRemoveNonExistingValue() {
        var map = new HashMap<Integer, String>();
        assertNull(map.remove(1), "Should return null");
    }
}