package com.learning.Datastructures.LanguageFeatures.Streams;

import com.learning.Datastructures.LanguageFeatures.Streams.IntegerStream;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class IntegerStreamsTest {

    @Test
    void testGetEvenNumbers() {
        var ints = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
        var evenSubset = IntegerStream.getEvenSubset(ints);
        assertEquals(3, evenSubset.size());
    }

    @Test
    void testGetUnevenNumbers() {
        var ints = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
        var unevenSubset = IntegerStream.getUnevenSubset(ints);
        assertEquals(4, unevenSubset.size());
    }

    @Test
    void testGetUnevenNumbersNoUnevenNumbers() {
        var ints = Arrays.asList(2, 4, 6, 8, 10);
        var unevenSubset = IntegerStream.getUnevenSubset(ints);
        assertEquals(0, unevenSubset.size());
    }

    @Test
    void testGetSum() {
        var ints = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
        assertEquals(28, IntegerStream.getSum(ints));
    }

    @Test
    void testGetProduct() {
        var ints = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
        assertEquals(5040, IntegerStream.getProduct(ints));
    }

    @Test
    void testGetDifference() {
        var ints = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
        assertEquals(-28, IntegerStream.getDifference(ints));
    }
}