from os import getcwd

import pytest
from selenium.webdriver.common.by import By


class TestResponsiveness:
    @pytest.mark.parametrize("window_width,grid_column", [("640", "2 / 12"), ("1024", "5 / 9")])
    def test_content_center_grid_column(self, window_width, grid_column, driver):
        path = "file://" + getcwd() + "/responsiveness/index.html"
        driver.get(path)
        driver.set_window_size(window_width, 480)
        el = driver.find_element(By.CSS_SELECTOR, ".content-center")
        assert el.value_of_css_property("grid-column") == grid_column

    @pytest.mark.parametrize("window_width, visibility", [(640, "hidden"), (1024, "visible")])
    def test_content_left_content_right_visibility(self, window_width, visibility, driver):
        path = "file://" + getcwd() + "/responsiveness/index.html"
        driver.get(path)
        driver.set_window_size(window_width, 480)
        left = driver.find_element(By.CSS_SELECTOR, ".content-left")
        right = driver.find_element(By.CSS_SELECTOR, ".content-right")
        assert left.value_of_css_property("visibility") == visibility
        assert right.value_of_css_property("visibility") == visibility
