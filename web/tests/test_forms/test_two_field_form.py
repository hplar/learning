from os import getcwd

import pytest
from selenium.webdriver.common.by import By


class TestTwoFieldForm:

    @pytest.mark.parametrize("data_1, data_2, expected", [
        ("Hello", "World", b"form-data-1=Hello&form-data-2=World"),
        (1, 2, b"form-data-1=1&form-data-2=2"),
    ])
    def test_form_submit_data(self, driver, httpserver, data_1, data_2, expected):
        path = "file://" + getcwd() + "/forms/two_field_form.html"
        driver.get(path)
        input_1 = driver.find_element(By.CSS_SELECTOR, "#form-data-1")
        input_2 = driver.find_element(By.CSS_SELECTOR, "#form-data-2")
        submit = driver.find_element(By.CSS_SELECTOR, "#submit-button")
        input_1.send_keys(data_1)
        input_2.send_keys(data_2)

        httpserver.expect_request("/", method="POST", query_string="form-data-1=Hello&form-data-2=World")
        submit.submit()
        assert httpserver.log[0][0].data == expected
