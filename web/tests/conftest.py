import pytest

from selenium import webdriver
from selenium.webdriver.firefox.options import Options


@pytest.fixture(scope="function")
def driver():
    options = Options()
    options.add_argument("--headless")
    driver = webdriver.Firefox(options=options)
    driver.set_window_size(640, 480)

    yield driver
    driver.quit()


@pytest.fixture(scope="session")
def httpserver_listen_address():
    """Override the default httpserver_listen_address fixture to use a custom port."""
    return "localhost", 9112
